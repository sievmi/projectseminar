from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, PasswordField, validators, SelectField
from wtforms.validators import Required

class LoginForm(Form):
    username = TextField('Username', validators = [Required()])
    password = PasswordField('Password', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    email = TextField('Email adress', [validators.Length(min=6, max=35)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Password must be match')
    ])
    confirm = PasswordField('Repeat password')


class PostForm(Form):
    body = TextField('Post body', validators = [Required()])

class DateForm(Form):
    month = SelectField('Month', choices=[('1', 'January'), ('2', 'February') , ('3', 'Marh'),
                                          ('4', 'April'), ('5', 'May'), ('6', 'June'), ('7', 'July'),
                                          ('8', 'August'),  ('9', 'September'),
                                          ('10', 'October'), ('11', 'November'), ('12', 'December') ])
    year = TextField('Year', validators = [Required()])

class AddResultForm(Form):
    day = TextField('Day', validators=[Required()])
    month = SelectField('Month', choices=[('1', 'January'), ('2', 'February') , ('3', 'Marh'),
                                          ('4', 'April'), ('5', 'May'), ('6', 'June'), ('7', 'July'),
                                          ('8', 'August'),  ('9', 'September'),
                                          ('10', 'October'), ('11', 'November'), ('12', 'December') ])
    year = TextField('Year', validators = [Required()])
    result = TextField('Result', validators = [Required()])
    habitname = TextField('HabitName', validators=[Required()])




