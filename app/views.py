from app import app, lm
from app.models import User,ROLE_USER,Post, Habit, HabitDay
from flask import render_template, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from forms import LoginForm, RegistrationForm, PostForm, DateForm, AddResultForm
from app import db
import datetime


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user

@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
@login_required
def index():
    user = g.user
    form = PostForm()
    if form.validate_on_submit():
        body = Post(body=form.body.data, timestamp=datetime.datetime.utcnow(), author = user)
        db.session.add(body)
        db.session.commit()
        return redirect(url_for('index'))
    posts = Post.query.all()
    return render_template('index.html',
        title = 'Home',
        user = user,
        posts = posts,
        form = form)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        user = User.query.filter_by(username = form.username.data).first()
        if user is None or (user.password != form.password.data):
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(request.args.get('next')) or url_for('index')

    return render_template('login.html',
        title = 'Sign In',
        form = form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/registration', methods=['GET', 'POST'])
def registration():
    if g.user is not None and g.user.is_authenticated:
        return reduce(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None:
            return redirect(url_for('index'))
        new_user = User(username=form.username.data, email=form.email.data, password=form.password.data, role = ROLE_USER);
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('registration.html',
                           title='Registration',
                           form = form)

@app.route('/<username>/notes')
@login_required
def notes(username):
    posts = Post.query.all()
    users_posts = []
    for p in posts:
        if p.author.username==username:
            users_posts.append(p)
    return render_template('notes.html',
                           title="Notes",
                           user = g.user,
                           posts = users_posts);

@app.route('/<username>/habits', methods=['GET', 'POST'])
@login_required
def habits(username):
    u = User.query.filter_by(username=username).all()[0]
    h = Habit.query.filter_by(author=u).all()
    habits = []
    for habit in h:
        curDate = datetime.datetime.today()
        curDay = datetime.datetime(year=curDate.year,month=curDate.month,day=curDate.day,hour=0,minute=0,second=0,
                              microsecond=0)
        start = curDay + datetime.timedelta(days=-2,hours=0,minutes=0,seconds=0,milliseconds=0,microseconds=0)
        end = curDay + datetime.timedelta(days=1,hours=0,minutes=0,seconds=0,milliseconds=0,microseconds=0)
        curDay = start
        results = []
        while(curDay != end):
            curHabbit = HabitDay.query.filter_by(username=username, habitname=habit.name, date=curDay).all()
            result = 0
            if curHabbit:
                result = 1
            results.append(str(result))
            curDay = curDay + datetime.timedelta(days=1,hours=0,minutes=0,seconds=0,milliseconds=0,microseconds=0)
        habits.append((habit.name, results))

    form = DateForm()
    result_form = AddResultForm()

    if result_form.validate_on_submit():
        date = datetime.datetime(year=int(result_form.year.data), month=int(result_form.month.data), day=int(result_form.day.data),
                                 hour = 0, minute=0,second=0,microsecond=0)
        result = int(result_form.result.data)
        habitname = result_form.habitname.data

        h = Habit.query.filter_by(name=habitname).all()
        if not h:
            u = User.query.filter_by(username=username).all()[0]
            hab = Habit(name=habitname,author=u)

        res = HabitDay.query.filter_by(username=username,habitname=habitname,date=date).all()
        if res:
            db.session.delete(res[0])
        if int(result):
            hd = HabitDay(username=username, habitname=habitname, date=date, result=result)
            db.session.add(hd)
        db.session.commit()
        return redirect('/'+username+'/habits')


    if form.validate_on_submit():
        u = User.query.filter_by(username=username).all()[0]
        h = Habit.query.filter_by(author=u).all()
        habits = []

        for habit in h:
            curDay = datetime.datetime(year=int(form.year.data), month=int(form.month.data), day=1, hour=0,minute=0,second=0,
                              microsecond=0)
            results = []
            month = curDay.month
            year = curDay.year
            while(month == curDay.month):
                curHabbit = HabitDay.query.filter_by(username=username, habitname=habit.name, date=curDay).all()
                result = 0
                if curHabbit:
                    result = 1
                results.append(str(result))
                curDay = curDay + datetime.timedelta(days=1,hours=0,minutes=0,seconds=0,milliseconds=0,microseconds=0)
            habits.append((habit.name, results))



        return render_template('habit_archive.html',
                               title='Archive',
                               user = g.user,
                               form = form,
                               habits = habits,
                               month=str(month),
                               year = str(year))




    return render_template('habits.html',
        title = 'Habits',
        user = g.user,
        form = form,
        result_form = result_form,
        habits = habits)

@app.route('/lists/<username>')
@login_required
def lists(username):
    return "LISTS"

@app.route('/to_do/<username>')
@login_required
def to_do(username):
    return "TO DO"
