from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
habit_day = Table('habit_day', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('result', SmallInteger, default=ColumnDefault(0)),
    Column('username', String(length=256)),
    Column('habitname', String(length=256)),
    Column('date', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['habit_day'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['habit_day'].drop()
